package co.com.test.demoblaze.tasks;

import co.com.test.demoblaze.interactions.AceptarVentanaEmergente;
import co.com.test.demoblaze.interactions.Wait;
import co.com.test.demoblaze.userinterfaces.PaginaInicio;
import co.com.test.demoblaze.utils.Constants;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class CrearCuenta implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Wait.theSeconds(3),
                Click.on(PaginaInicio.SIGN_UP_BOTON),
                Wait.theSeconds(3),
                Enter.theValue(Constants.NOMBRE_USUARIO).into(PaginaInicio.CAMPO_USERNAME),
                Wait.theSeconds(3),
                Enter.theValue(Constants.CONTRASENA).into(PaginaInicio.CAMPO_PASSWORD),
                Wait.theSeconds(3),
                Click.on(PaginaInicio.CREAR_CUENTA),
                Wait.theSeconds(3),
                AceptarVentanaEmergente.aceptarVentanaEmergente(),
                Wait.theSeconds(3)

        );
    }
    public static CrearCuenta crearCuenta(){
        return instrumented(CrearCuenta.class);
    }
}
