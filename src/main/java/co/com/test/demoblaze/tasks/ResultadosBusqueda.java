package co.com.test.demoblaze.tasks;

import co.com.test.demoblaze.interactions.Wait;
import co.com.test.demoblaze.userinterfaces.PaginaInicio;
import co.com.test.demoblaze.userinterfaces.PaginaLaptop;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import static net.serenitybdd.screenplay.Tasks.instrumented;


public class ResultadosBusqueda implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Wait.theSeconds(5),
                Click.on(PaginaInicio.LAPTOP),
                Wait.theSeconds(4),
                Click.on(PaginaLaptop.LAPTOP_DELL),
                Wait.theSeconds(4)
        );

    }
    public static ResultadosBusqueda busqueda(){
        return instrumented(ResultadosBusqueda.class);
    }
}
