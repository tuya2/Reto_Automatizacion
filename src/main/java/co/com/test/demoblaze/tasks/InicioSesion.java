package co.com.test.demoblaze.tasks;

import co.com.test.demoblaze.interactions.Wait;
import co.com.test.demoblaze.userinterfaces.PaginaInicio;
import co.com.test.demoblaze.utils.Constants;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class InicioSesion implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Wait.theSeconds(3),
                Click.on(PaginaInicio.LOG_IN_BOTON),
                Wait.theSeconds(3),
                Enter.theValue(Constants.NOMBRE_USUARIO).into(PaginaInicio.USUARIO_LOG_IN),
                Wait.theSeconds(3),
                Enter.theValue(Constants.CONTRASENA).into(PaginaInicio.PASSWORD_LOG_IN),
                Wait.theSeconds(3),
                Click.on(PaginaInicio.INICIAR_SESION),
                Wait.theSeconds(4)
        );
    }
    public static InicioSesion inicioSesion(){
        return instrumented(InicioSesion.class);
    }
}
