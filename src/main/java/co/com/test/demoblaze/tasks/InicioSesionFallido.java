package co.com.test.demoblaze.tasks;

import co.com.test.demoblaze.interactions.AceptarVentanaEmergente;
import co.com.test.demoblaze.interactions.Wait;
import co.com.test.demoblaze.userinterfaces.PaginaInicio;
import co.com.test.demoblaze.utils.Constants;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class InicioSesionFallido implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Wait.theSeconds(3),
                Click.on(PaginaInicio.LOG_IN_BOTON),
                Wait.theSeconds(3),
                Enter.theValue(Constants.USUARIO_FALLIDO).into(PaginaInicio.USUARIO_LOG_IN),
                Wait.theSeconds(3),
                Enter.theValue(Constants.CONTRASENA_FALLIDO).into(PaginaInicio.PASSWORD_LOG_IN),
                Wait.theSeconds(3),
                Click.on(PaginaInicio.INICIAR_SESION),
                Wait.theSeconds(3),
                AceptarVentanaEmergente.aceptarVentanaEmergente(),
                Wait.theSeconds(3)
        );
    }
    public static InicioSesionFallido inicioSesionFallido(){
        return instrumented(InicioSesionFallido.class);
    }
}
