package co.com.test.demoblaze.questions;

import co.com.test.demoblaze.userinterfaces.LaptopDell;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class ValidarPrecio implements Question {
    @Override
    public Object answeredBy(Actor actor) {
        return LaptopDell.LAPTOP_PRECIO.resolveFor(actor).getTextContent().contains("$700");
    }
    public static Question <Boolean> ValidarPrecio() {
        return new ValidarPrecio();
    }
}
