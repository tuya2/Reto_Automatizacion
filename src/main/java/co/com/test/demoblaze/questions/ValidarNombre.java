package co.com.test.demoblaze.questions;

import co.com.test.demoblaze.userinterfaces.LaptopDell;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class ValidarNombre implements Question {
    @Override
    public Object answeredBy(Actor actor) {
        return LaptopDell.LAPTOP_NOMBRE.resolveFor(actor).getText().equals("Dell i7 8gb");
    }
    public static Question<Boolean> ValidarNombre() {
        return new ValidarNombre();
    }
}
