package co.com.test.demoblaze.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;

public class LaptopDell {
    public static final Target LAPTOP_NOMBRE = Target.the("Click en laptop").
            locatedBy("//h2[contains(text(),'Dell i7 8gb')]");
    public static final Target LAPTOP_PRECIO = Target.the("Click en laptop").
            locatedBy("//h3[contains(text(),'$700')]");
    public static final Target LAPTOP_DESCRIPCION = Target.the("Click en laptop").
            locatedBy("//strong[contains(text(),'Product description')]");

}
