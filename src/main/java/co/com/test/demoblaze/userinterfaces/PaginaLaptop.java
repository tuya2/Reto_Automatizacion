package co.com.test.demoblaze.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;

public class PaginaLaptop {
    public static final Target LAPTOP_DELL = Target.the("Click en laptop dell i7 8gb").
            locatedBy("//a[contains(text(),'Dell i7 8gb')]");
    public static final Target LAPTOP = Target.the("Click en laptop").
            locatedBy("//a[contains(text(),'Laptops')]");
}
