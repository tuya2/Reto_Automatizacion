package co.com.test.demoblaze.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/resultados_busquedas.feature",
        glue = "co.com.test.demoblaze.stepdefinitions",
        snippets = SnippetType.CAMELCASE)
public class ResultadosBusqueda {

}
