Feature: Yo como cliente del almacén demoblaze
  Necesito visualizar los precios de los productos de la categoría Laptops
  Para tomar decisiones de compra


  Background:
    Given el usuario ingresa a la pagina
    When el usuario consulta un laptop

  @busquedaExitosa
  Scenario: Resultado de busqueda exitoso
    Then el usuario ve el nombre del producto
    And el usuario ve el precio del producto
    And el usuario ve la descripcion del producto


